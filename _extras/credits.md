---
title: "Credits for External Materials"
---

## Image Credits

* Citation for turbine (from Outro):

Medic, G. & Kalitzin, G. & You, D. & Herrmann, Marcus & Ham, Frank & Weide, Edwin & Pitsch, H. & Alonso, Juan. (2006). "Integrated RANS/LES computations of turbulent o w through a turbofan jet engine." Annual Research Briefs 2006. <https://www.researchgate.net/publication/252644683_Integrated_RANSLES_computations_of_turbulent_o_w_through_a_turbofan_jet_engine>.

* Citation for helium detonation (from Outro):

M. Zingale et al., “Helium Detonations on Neutron Stars,” The Astrophysical Journal Supplement Series, vol. 133, no. 1, pp. 195–220, Mar. 2001, doi: https://doi.org/10.1086/319182.

B. Fryxell et al., “FLASH: An Adaptive Mesh Hydrodynamics Code for Modeling Astrophysical Thermonuclear Flashes,” The Astrophysical Journal Supplement Series, vol. 131, no. 1, pp. 273–334, Nov. 2000, doi: https://doi.org/10.1086/317361.

* Citation for brain scan (from Outro):

C.-H. Chang, X. Yu and J. X. Ji, "Compressed sensing MRI reconstruction from 3D multichannel data using GPUs", Magn. Reson. Med., vol. 78, no. 6, pp. 2265-2274, 2017. https://doi.org/10.1002/mrm.26636

Hamilton, Jesse et al. “Recent advances in parallel imaging for MRI.” Progress in nuclear magnetic resonance spectroscopy vol. 101 (2017): 71-95. doi:10.1016/j.pnmrs.2017.04.002

* Citation for ocean model (from Outro):

Petersen, M. R., Asay‐Davis, X. S., Berres, A. S., Chen, Q., Feige, N., Hoffman, M. J., et al. (2019). An evaluation of the ocean and sea ice climate of E3SM using MPAS and interannual CORE‐II forcing. Journal of Advances in Modeling Earth Systems, 11, 1438– 1458. https://doi.org/10.1029/2018MS001373

* Citation for internet model (from Outro):

Lyon, Barrett. "The Opte Project." Mapping the Internet, Self, 2003-2022. http://www.opte.org.

LGL-Large Graph Layout. "The Opte Project." Github: 2003-2022. https://github.com/TheOpteProject/LGL.

* Citation for geoscience (from Outro):

Harvard Seismic Velocity Models. Harvard Seismology. Harvard University: Department of Earth and Planetary Sciences. Cambridge. http://www.seismology.harvard.edu/resources.html

Geomap: Research in Progress. Harvard Seismology: Jan 2002. https://web.archive.org/web/20091005085107/http://www.seismology.harvard.edu/projects/geomap/geomap.html

* Citation for plane decomposition

Lee-Rausch, Beth. Application 53. 3D Domain Decomposition. NASA FUN3D: Fully Unstructured Navier-Stokes. July 2024. https://fun3d.larc.nasa.gov/example-53.html.
  
{% include links.md %}
