DeapSECURE Hands-on Files for `mpi4py` (Published Version)
==========================================================

Files located in this directory include Python programs to demonstrate
basic capabilities of MPI library.

These files should be synced with those from `/devel/handson/mpi4py`
repository of the
[DeapSECURE developer's repo for PAR module](
    https://gitlab.com/deapsecure/deapsecure-mod06-par-devel/
).
