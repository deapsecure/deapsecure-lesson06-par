#!/usr/bin/env python3
from mpi4py import MPI

comm = MPI.COMM_WORLD    # initialize the communicator
size = comm.Get_size()   # get number of processes
rank = comm.Get_rank()   # get unique ID for *this* process

print("Hello from process ", rank, " part of a pool of ", size, " processes.")
