# DeapSECURE Lesson Module: Parallel and High-Performance Computing

<!-- FIXME

{% comment %}
[![Create a Slack Account with us](https://img.shields.io/badge/Create_Slack_Account-The_Carpentries-071159.svg)](https://swc-slack-invite.herokuapp.com/)
{% endcomment %}

-->

This is an open-source lesson for the
[DeapSECURE training program](https://deapsecure.gitlab.io/),
a computational training for cybersecurity research & education.

For the rendered version of this lesson,
please go to
<https://deapsecure.gitlab.io/deapsecure-lesson06-par/> .

## Contributing

We welcome all contributions to improve the lesson! Maintainers will do their best to help you if you have any
questions, concerns, or experience any difficulties along the way.

We'd like to ask you to familiarize yourself with our [Contribution Guide](CONTRIBUTING.md) and have a look at
the [more detailed guidelines][lesson-example] on proper formatting, ways to render the lesson locally, and even
how to write new episodes.


## Maintainer(s)

DeapSECURE lesson modules are being maintained by the DeapSECURE Team at the Old Dominion University.
You are strongly encouraged to reach the team using the public Gitlab issues,
where appropriate (i.e. for issues with the lesson materials, discussions, ideas, feedback, etc.).
For inquiries and other matters, please reach out to:

* Wirawan Purwanto <wpurwant@odu.edu>


## Authors

A list of contributors to the lesson can be found in [AUTHORS](AUTHORS)


## Citation

To cite this lesson, please consult with [CITATION](CITATION)

[lesson-example]: https://carpentries.github.io/lesson-example


## Credits

Our
[credit page](https://deapsecure.gitlab.io/deapsecure-lesson06-par/credits/index.html)
contains the list of materials cited, used, or adapted for this lesson
as well as credit to their respective authors.

This lesson was typeset on
[The Carpentries style](https://github.com/carpentries/styles/)
version [9.5.3](https://github.com/carpentries/styles/releases/tag/v9.5.3).

