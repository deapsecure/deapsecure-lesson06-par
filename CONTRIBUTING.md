# Contributing

[DeapSECURE][deapsecure-site] is an open source project
of the School of Cybersecurity at Old Dominion University,
and we welcome contributions of all kinds:
new lessons,
fixes to existing material,
bug reports,
and reviews of proposed changes are all welcome.

## Contributor Agreement

By contributing,
you agree that we may redistribute your work under [our license](LICENSE.md).
In exchange,
we will address your issues and/or assess your change proposal as promptly as we can,
and help you become a member of our community.
Everyone involved in the [DeapSECURE][deapsecure-site] project
agrees to abide by our [code of conduct](CODE_OF_CONDUCT.md).

## How to Contribute

The easiest way to get started is to file an issue
to tell us about a spelling mistake,
some awkward wording,
or a factual error.
This is a good way to introduce yourself
and to meet some of our community members.

1.  If you do not have a [GitLab][gitlab] account,
    you can [send us comments by email][email].
    However,
    we will be able to respond more quickly if you use one of the other methods described below.

2.  If you have a [GitLab][gitlab] account,
    or are willing to [create one][gitlab-join],
    but do not know how to use Git,
    you can report problems or suggest improvements by [creating an issue][gitlab-issues].
    This allows us to assign the item to someone
    and to respond to it in a threaded discussion.

3.  If you are comfortable with Git,
    and would like to add or change material,
    you can submit a merge request (also more popularly called *pull request*, or PR).
    Instructions for doing this are [included below](#using-gitlab).

## Where to Contribute

1.  If you wish to change this lesson,
    please access the official lesson repo in <https://gitlab.com/deapsecure/deapsecure-lesson06-par>,
    which can be viewed at <https://gitlab.com/deapsecure/deapsecure-lesson06-par/>.
    Please see the "Using Gitlab" section below for more detailed instructions.

2.  This DeapSECURE lesson uses the [Carpentries lesson template][carpentries-template].
    If you wish to change the example lesson,
    please work in <https://github.com/carpentries/lesson-example>,
    which documents the format of our lessons
    and can be viewed at <https://carpentries.github.io/lesson-example>.

    <!-- FIXME for the point above -->

<!-- Not applicable to DeapSECURE

3.  If you wish to change the template used for workshop websites,
    please work in <https://github.com/carpentries/workshop-template>.
    The home page of that repository explains how to set up workshop websites,
    while the extra pages in <https://carpentries.github.io/workshop-template>
    provide more background on our design choices.

-->

3.  If you wish to change CSS style files, tools,
    or HTML boilerplate for lessons or workshops stored in `_includes` or `_layouts`,
    please work in <https://github.com/carpentries/styles>.

    <!-- FIXME for the point above -->

## What to Contribute

Since DeapSECURE is still a new project, we are looking for community input
in terms of the completeness of the lesson materials.
Any missing subtopics, or any suggestion for a new episode is still welcome
at this point in time.
Please file an issue to discuss these feedback and ideas.

There are many ways to contribute,
from writing new exercises and improving existing ones
to updating or filling in the documentation
and submitting [bug reports][issues]
about things that don't work, aren't clear, or are missing.
If you are looking for ideas, please see the 'Issues' tab for
a list of issues associated with this repository.

<!-- FIXME
or you may also look at the issues for [DeapSECURE][deapsecure-issues--FIXME]
projects.
-->

Comments on issues and reviews of pull requests are just as welcome:
we are smarter together than we are on our own.
Reviews from novices and newcomers are particularly valuable:
it's easy for people who have been using these lessons for a while
to forget how impenetrable some of this material can be,
so fresh eyes are always welcome.

As a rule, if you want to introduce a new idea,
you must (a) estimate how long it will take to teach
and (b) explain what you would take out to make room for it, if applicable,
to keep the lesson at a manageable length.
The first encourages contributors to be honest about requirements;
the second, to think hard about priorities.


## What *Not* to Contribute

<!-- commented out original paragraph

Our lessons already contain more material than we can cover in a typical workshop,
so we are usually *not* looking for more concepts or tools to add to them.
As a rule,
if you want to introduce a new idea,
you must (a) estimate how long it will take to teach
and (b) explain what you would take out to make room for it.
The first encourages contributors to be honest about requirements;
the second, to think hard about priorities.

-->

We are not looking for exercises or other material that only run on one platform.
Our workshops typically contain a mixture of Windows, macOS, and Linux users;
in order to be usable,
our lessons must run equally well on all three.
In particular, these exercises must also run well on standard HPC platforms.
Also consider the fitness of these exercises on cloud platforms.

## Using Gitlab

If you choose to contribute via Gitlab, you may want to look at
[How to Contribute to an Open Source Project on GitHub][how-contribute]
*(Note: This is a GitHub-specific guideline,
but many things and concepts in GitHub sphere translate to Gitlab).*
To manage changes, we follow [GitHub flow][github-flow].
Each lesson has two maintainers who review issues and pull requests or encourage others to do so.
The maintainers are community volunteers and have final say over what gets merged into the lesson.
To use the web interface for contributing to a lesson:

1.  Fork the originating repository to your Gitlab profile.
2.  Within your version of the forked repository, move to the `gh-pages` branch and
create a new branch for each significant change being made.
3.  Navigate to the file(s) you wish to change within the new branches and make revisions as required.
4.  Commit all changed files within the appropriate branches.
5.  Create individual pull requests from each of your changed branches
to the `gh-pages` branch within the originating repository.
6.  If you receive feedback, make changes using your issue-specific branches of the forked
repository and the pull requests will update automatically.
7.  Repeat as needed until all feedback has been addressed.

When starting work, please make sure your clone of the originating `gh-pages` branch is up-to-date
before creating your own revision-specific branch(es) from there.
Additionally, please only work from your newly-created branch(es) and *not*
your clone of the originating `gh-pages` branch.
Lastly, published copies of all the lessons are available in the `gh-pages` branch of the originating
repository for reference while revising.

## Other Resources

General discussion of [DeapSECURE][deapsecure-site]
happens on [DeapSECURE - Cyberinfrastructure training for cybersecurity research][deapsecure-dev-slack],
which is currently a private Slack channel.
Interested people are welcome to join by contacting us via email first.
You can also [reach us by email][email].

[email]: mailto:deapsecure@odu.edu
[dc-issues]: https://github.com/issues?q=user%3Adatacarpentry
[dc-lessons]: http://datacarpentry.org/lessons/
[dc-site]: http://datacarpentry.org/
[discuss-list]: http://lists.software-carpentry.org/listinfo/discuss
[github]: https://github.com
[github-flow]: https://guides.github.com/introduction/flow/
[github-join]: https://github.com/join
[how-contribute]: https://egghead.io/series/how-to-contribute-to-an-open-source-project-on-github
[issues]: https://guides.github.com/features/issues/
[swc-issues]: https://github.com/issues?q=user%3Aswcarpentry
[swc-lessons]: https://software-carpentry.org/lessons/
[swc-site]: https://software-carpentry.org/
[carpentries-template]: https://github.com/carpentries/styles
[deapsecure-site]: https://deapsecure.gitlab.io/
[deapsecure-dev-slack]: https://app.slack.com/client/TEDK0V6MU/CED8R89EK
[gitlab]: https://gitlab.com
[gitlab-join]: https://gitlab.com/users/sign_up
[gitlab-issues]: https://docs.gitlab.com/ee/user/project/issues/
