---
layout: reference
---

## Glossary

## References

### OpenMP

* [OpenMP main website](https://www.openmp.org) ---
  containing pointers to
  [learning resources](https://www.openmp.org/resources/tutorials-articles/),
  [reference guides](https://www.openmp.org/resources/refguides/),
  [specifications](https://www.openmp.org/specifications/),
  and many more.

* [OpenMP Tutorial][omp_tut_llnl] by Blaise Barney.
  Excellent tutorial for getting started with OpenMP parallel computing.

[omp_tut_llnl]: https://computing.llnl.gov/tutorials/openMP/
{% include links.md %}
