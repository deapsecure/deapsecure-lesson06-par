---
title: "A Computer Model for Physical Unclonable Function"
teaching: 0
exercises: 0
questions:
- "Key question (FIXME)"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "First key point. Brief Answer to questions. (FIXME)"
---

In this episode, we will learn a computer model for
Physical Unclonable Functions as proposed by Yu and Kose
(W. Yu and S. Kose, 
"A Lightweight Masked AES Implementation for Securing IoT Against CPA Attacks",
IEEE Transactions on Circuits and Systems I, vol. 64, no. 11, pp. 2934-2944, 2017).

A simple resistant PUF design proposed by Yu and Chen:

![Three designs of PUF chip](../fig/PUF-chip-designs.png)

(Credit: Yu and Chen, arxiv.cs 1810.0725v1)

In this workshop, we will focus for a computer code that will simulate only the
PUF block in the picture above.

{% include links.md %}
