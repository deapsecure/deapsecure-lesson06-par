---
title: "PUF Modeling: Sequential Program"
teaching: 0
exercises: 0
questions:
- "Key question (FIXME)"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "Intensive computation with compiled languages like C can yield tremendous speedup."
---

The PUF model described in the previous episode is easy to implement
with Matlab, Python, and C programming languages.

We will work with a program that is meant to generate responses from
this PUF model.
We feed the PUF with randomly created 128-bit integers, and record the
128-bit response from each of these integers.



## C Implementation of PUF Model

The following snippet shows the main part of the PUF code in C
programing language.

```c
   #define NUM_BITS 128
   ...
   int epoches = 200000;
   ...
   puf_generate(NUM_BITS, puf_str_a);
   puf_generate(NUM_BITS, puf_str_b);

   for(i = 0; i < epoches; i++)
   {
      init_challenge(NUM_BITS, challenge);

      for(j = 0; j < NUM_BITS; j++)
      {
         element_multiply(NUM_BITS, puf_str_a[j], challenge, tmp);
         comparison[0] = mat_sum(NUM_BITS, tmp);
         element_multiply(NUM_BITS, puf_str_b[j], challenge, tmp);
         comparison[1] = mat_sum(NUM_BITS, tmp);
         response[j] = (int)ceil((comparison[0] - comparison[1]) / 128);
      }

      memcpy(puf1_data[0] + i * NUM_BITS, challenge, NUM_BITS * sizeof(int));
      memcpy(puf1_data[1] + i * NUM_BITS, response, NUM_BITS * sizeof(int));

      if(i % 2000 == 0)
      {
         printf("Epoch %d\n", i);
      }
   }
   ...
   sprintf(filename, "./puf1_result.txt");
   file = fopen(filename, "w");
   for(i = 0; i < epoches; i++)
   {
      for(j = 0; j < NUM_BITS; j++)
         fprintf(file, "%d ", puf1_data[0][j]);
      fprintf(file, "\n");
   }

   for(i = 0; i < epoches; i++)
   {
      for(j = 0; j < NUM_BITS; j++)
         fprintf(file, "%d ", puf1_data[1][j]);
      fprintf(file, "\n");
   }
   fclose(file);

```
Recall that our PUF operates on 128-bit quantities, therefore `NUM_BITS` is 128.

The program does the following:

1. First, it creates two 128x128 matrices A and B (stored as
   `puf_str_a` and `puf_str_b` above) and initializes them with random
   values between 0 and 1.  In the current model, these matrices
   represent the "physical circuits" within the PUF.

2. Second, it generates 200000 random integers (stored in `challenge`)
   and computes the PUF response for each integer (stored in `response`).
   The program prints out the iteration number every 2000 iterations.

3. The generated challenges are stored in `puf1_data[0]`, while
   the gathered responses in `puf1_data[1]`.

4. At the end, the dump the generated challenges and responses.

In our current implementation,
a "128-bit integer" is actually stored as an "array of 128 integers".
The `challenge` and `response` variables are such arrays.

Here are some key routines:

* `init_challenge` generates a new challenge by filling in the `challenge`
  array with 128 random 0's and 1's.

* `element_multiply` performs elementwise multiplication of the two input arrays.
  For example:

  ```c
  element_multiply(NUM_BITS, puf_str_a[j], challenge, tmp);
  ```

  multiplies the `j`-th row of the A matrix with the `challenge` array,
  element-by-element.
  The result is stored in the `tmp` array.

* `vec_sum` performs a sum of the input array values.

(Taken together, the combination `element_multiply` and `vec_sum` constitute
a vector dot product.)

The complete program is located in your hands-on directory,
`puf-generation/puf.c`.


Let us now compile the program and run it (remember to do this on a compute node):

```bash
$ gcc -o puf puf.c -lm
$ ./puf
```

Here is the output of the execution of the program:
```
[wpurwant@coreV3-23-012 puf-generation]$ ./puf
Epoch 0
Epoch 2000
Epoch 4000
Epoch 6000
Epoch 8000
Epoch 10000
...
Epoch 192000
Epoch 194000
Epoch 196000
Epoch 198000
Total time 47 s CPU, 47 s elapsed
```
{: .output}

The program prints both the CPU time and elapsed time.
"CPU time" refers to the amount of time the CPU (core) is busy doing work.
"Elapsed time" refers to the actual elapsed time as measured by a clock;
it is also frequently referred to as *wallclock time*.
At this point, both are identical, because the CPU is busy for the entire loop.
The difference will become apparent when multiple cores are used later.

The time reported above are measured from the beginning of the first iteration
to the end of the last iteration of the `for` loop.
It does not measure the additional time taken at the end of the program to dump
the challenge-response arrays.

> ## C vs Matlab: How Faster is Fast?
>
> The original PUF code was written in Matlab.
> In Matlab, the same calculation took about 180 seconds,
> whereas here it took less than one minute.
> This demonstrates the point that compiled languages like C has the potential
> of yielding significant speedup when compared to interpreted languages like
> Matlab or Python.
> The reason is that the C program is compiled directly into machine instructions,
> which can be executed directly by the CPU.
> Matlab program, however, requires a lot of layers to interpret the commands.
> Array and matrix operations in Matlab are done using programs written in
> machine languages (i.e. compiled from C or C++).
> Therefore, when handling computation of very large arrays using Matlab-provided
> functions, the extra cost of interpretation may be minimized.
> Hand-written loops, on the other hand, are almost always bad because their execution
> requires intensive work for interpreting the commands.
>
> In our case, compiled C code runs three times faster then Matlab code that does
> the same thing.
> In general, the speedup of the code can range between 3--50 times, depending
> on the complexity of the code.
{: .callout}

{% include links.md %}
