---
title: "Shared Memory Model with OpenMP"
teaching: 10
exercises: 20
questions:
- What is OpenMP?
- How to write our first program with OpenMP?
objectives:
- Get functional knowledge of shared-memory model with OpenMP
keypoints:
- "OpenMP is a directive-based way to parallelize a sequential program to use multiple core on a shared-memory machine."
---

## Brief history of OpenMP

`OpenMP` as stated before is a shared-memory application programming interface.
OpenMP stands for `Open Multi-Processing`.
OpenMP is compatible with several operating systems such as Windows, MacOS, Linux to name a few.
Languages supported by OpenMP are C/C++ and FORTRAN.
OpenMP is not compatible with Python, this is the reason why we will be using C for this part of the workshop.
The first specification of OpenMP was published in 1997 as OpenMP version 1, only compatible with FORTRAN.
The following year C/C++ implementation was released.
Version 2.0 of the OpenMP specification came in the year 2000, but only for FORTRAN again, with version 2.0 for C/C++ coming two years later.
In 2005 a joint C/C++/FORTRAN specification of OpenMP was published as version 2.5.
Later on several versions were released, today, we are at version 5.0 released in 2018.
OpenMP was originally geared toward highly regular loops, but eventually the
specifications included more complex constructs to make OpenMP what it is today.
For example, OpenMP now supports task-based parallelism (where every thread may
do different kinds of work from its peers) as well as accelerators such as GPU
(graphical processing unit) and FPGA (field-programmable gate array).

OpenMP is easy to use but requires some practice to master.
Unlike MPI, where the data and/or task distribution has to be explicitly programmed,
OpenMP uses directives that look like comment lines, to indicate how parts of
the program should be parallelized.
In this section, we will learn some of the basic constructs of the specification.
In an upcoming section, we will learn how to parallelize a real program with OpenMP.


![Serial and parallel regions of OpenMP program](../fig/fork_join.png)
<p align="center">Figure: Illustration of an OpenMP program. The initial process spawns threads in a parallel region and has the ability to go back to a single process after exiting the region.</p>

The threads are numbered from 0, 1, 2, ... (N-1), where N is the total
number of threads with which the program is executed.


## What Does an OpenMP Program Look Like?

The following is our first OpenMP program written in C:

```c
#include <stdio.h>
#include <omp.h>

int main(void)
{
   #pragma omp parallel
   printf("Hello, world.\n");
   return 0;
}
```
> ## Program Outcome
> What is the expected outcome when the program above is executed?
>
>> ## Hint:
>> Each thread will print a string: "Hello, world" to the output.
> {:.solution}
{:.challenge}

Our first OpenMP program does only one thing:
for each thread, it prints a string message to the output.
The entire program is written in standard C with the exception of one line:

```c
# pragma omp parallel
```

OpenMP program constructs are simply compiler directives that begin with
`#pragma omp`.
These are instructions to give to compiler regarding how to parallelize
a certain section of an otherwise serial code.
More specifically, the `#pragma omp parallel` directive informs the compiler
that the following line (or section of the code delimited by `{` and `}`)
is to be run in parallel (concurrently) by multiple threads.
(We will see later how to have more than a line in our parallel block.)
Other OpenMP directives are also used to perform several functions with threads;
for example, to synchronize threads or to declare thread variables.

> ## Directive-based Programming
> There is another popular directive-based programming inspired by OpenMP,
> called "OpenACC".
> It has a similar directive prefix: `#pragma acc`.
> The difference is that OpenMP was originally designed to run on CPUs,
> whereas OpenACC was designed with accelerators (primarily GPUs) in mind.
{: .callout}

## Compiling and running OpenMP programs

Compilation is the act of converting a program from a programming language (like C/C++) to a more machine readable code that the computer can understand (binary code).
To compile a program we need to use a tool called `compiler`.
Each language has its own compiler and there are many versions for each compiler from different sources, such as GNU, Intel...
For our case we will use a C compiler from GNU software foundation `cc`.
Compilers require some flags to know which libraries to add in order to link the different functions of the program as needed.
For OpenMP, GNU compiler provides the flag `-fopenmp`.
Another flag we are using is `-o` to direct the compiler to save the executable under a name following the `-o` flag.

```bash
cc -o program.exe program.c -fopenmp
./program.exe
```

```
   Hello, world.
   Hello, world.
   Hello, world.
   Hello, world.
```
{: .output}

### Setting the number of threads: `OMP_NUM_THREADS` environment variable and `omp_set_num_threads` function

The number of threads to use in parallel regions can be set prior to the program using the environment variable `OMP_NUM_THREADS`, from within your allocation, in your job script.
To do so on Turing you can use the shell command:

```bash
setenv OMP_NUM_THREADS 4
```

> ## If you use bash instead of tcsh use:
>
>export OMP_NUM_THREADS=4
>
{:.callout}

### Number of threads in a parallel region: omp_get_num_threads()

 An important information that can have different uses while running an OpenMP application is the number of threads being used.
 This information can change from execution to execution, by environment variable or from within the program itself.
 To know exactly how many threads are used, simply use the `omp_get_num_threads` function as in the program bellow.

 ```c
#include <stdio.h>
#include <omp.h>

int main(void)
{
   int num_threads;
   #pragma omp parallel
   {
      num_threads = omp_get_num_threads();
      printf("Hello world, we have %d threads running!\n", num_threads);
   }
   return 0;
}
```

### Thread id: omp_get_thread_num(), private directive
OpenMP gives a unique identification number to each of the threads running for each execution session.
This ID could be used to choose a thread or more to run a given part of the program, or to separate master and workers.
To determine a thread ID, we have OpenMP function `omp_get_thread_num`.

```c
 #include <stdio.h>
 #include <omp.h>

 int main(void)
 {
     int tid;
     #pragma omp parallel private(tid)
     {
         tid = omp_get_thread_num();
         printf("Hello world, I am thread %d!\n", tid);
     }
     return 0;
 }
```

 ```c
    #pragma omp parallel private(tid)
 ```

### Thread synchronization: barrier directive
Sometimes, some parts of a parallel program need all threads to be done with a previous task before starting the next.
Such a task requires synchronization of all threads before continuing.
To do so, there is a special OpenMP directive for the job.

```c
 #include <stdio.h>
 #include <omp.h>

 int main(void)
 {
     int tid;

     #pragma omp parallel private(tid)
     {
         tid = omp_get_thread_num();

         #pragma omp barrier
         printf("Hello world, I am thread %d!\n", tid);
     }
     return 0;
 }
```
In the above listing, we can see a line with the `barrier` directive:

```c
    #pragma omp barrier
```

What hapens in this code is as soon as the threads execute the line:

```c
   tid = omp_get_thread_num();
```
They stop and wait for all other thread to catch up.
The moment all threads have reached the `barrier` directive, the next line in the code can be executed, all threads can move on.

### Parallel loop : the for directive

The `#pragma for` directive is a very powerful.
It allows the compiler to take over more than just running the region in parallel, but also making sure that each iteration in the region is done once by sharing the number of iterations, thus work among the threads.

```c
#include <stdio.h>
#include <omp.h>

int main(void)
{
   int tid;
   int a[20];
   int i;

   //init a to -1 for all values
   for(i = 0; i < 20; i++)
      a[i] = -1;

   //print a
   for(i = 0; i < 20; i++)
      printf("a[%d] = %d\n", i, a[i]);

   //Work in parallel each thread replaces its portion of a by by its tid
   #pragma omp parallel private(tid)
   {
      tid = omp_get_thread_num();
      printf("Hello world, I am thread %d!\n", tid);

      #pragma omp for
      for(i = 0; i < 20; i++)
      {
         a[i] = tid;
      }
   }

   //print a again to check values
   for(i = 0; i < 20; i++)
      printf("a[%d] = %d\n", i, a[i]);
   return 0;
}
```


## Hands-on: Sum of numbers across OpenMP threads
King Oofy has decided to add parallel programming in the curriculum of senior highschoolers in his kingdom.
Although he is the king, he is so invested into finding interesting ideas, even for education in the kingdom.
Due to his busy schedule, he comes up with ideas and then hand them over to his subjects for further development.
Our dear king has chosen you to come up with an implementation for the following problem:

Given a set of n workers with IDs 0,1,2,..., n-1 write a shared-memory parallel program where each worker will compute an array of integers containing 0, r, 2r, 3r with r being the worker's ID (going from 0 to n-1).
This leads to the following series: 0, 0, 0, 0, 0, 1, 2, 3, 0, 2, 4, 6, 0, 3, 6, 9 ...

|-------------|--------------|
|  worker id  |     series   |
|-------------|--------------|
|     0       |    0,0,0,0   |
|     1       |    0,1,2,3   |
|     2       |    0,2,4,6   |
|     3       |    0,3,6,9   |
|    ...      |      ...     |
|-------------|--------------|

After computing the series, calculate the sum of all integers as the final result.

To make this simple, you decided to implement a solution for 4 threads.
Your series will look like in the previous table and your final result is 36.

First thing first, you need header files

```c
    #include <stdio.h>
    #include <omp.h>
```

Second, your main program:

```c
    #include <stdio.h>
    #include <omp.h>


    int main()
    {

         return 0;
    }
```
The next things you need are some variables:

 * the number of workers `num_threads`
 * the array of integers accessible by every worker `array` of size 4x4 integers
 * the id of each worker `tid`
 * the sum of all integers `sum`
 * an iterator variable `i`

Here is what your code looks like now:

```c
    #include <stdio.h>
    #include <omp.h>


    int main()
    {
         int num_threads;
         int array[16];
         int tid;
         int sum;
         int i;

         return 0;
    }
```

The best way to do this is to use a `for` loop.
Recall that in a for loop, openMP splits the data in the most evenly way possible.
Here since we use 4 threads, and our data is of size 16, each thread will have 4 integers to work with.
Thread 0 will have array[0] through array[3], thread 1 will have array[4] through array[7] and so on with other threads.
Each thread has to fill its part as in the above table.
For thread 0, this is simple for each i, fill i*tid (0*0, 1*0, 2*0, 3*0).
For thread 1, i = 4, 5, 6, 7 this is more complicated.
Observe that i = 4 + 0, 4 + 1, 4 + 2, 4 + 3 for thread 1.
For thread 2 you have  i = 8 + 0, 8 + 1, 8 + 2, 8 + 3...
You need to extract the right side of the `+` for each i.
For thread 0, i = 0 + 0,1,2,3, for thread 1, i = 4 + 0,1,2,3, for thread 2, i = 8 + 0,1,2,3, and for thread 3, i = 12 + 0,1,2,3.
Notice that the left side is always divisible by 4, this means that the part we want to extract is the remainder of i/4.
To get what we want we just need to get the remainder of i/4, we can use the expression i%4.
Now here is what your code looks like:


```c
#include <stdio.h>
#include <omp.h>

int main()
{
   int num_threads;
   int array[16];
   int tid;
   int sum;
   int i;

   //to fill array
   #pragma omp parallel private(tid)
   {
      //Get my thread id
      tid = omp_get_thread_num();

      #pragma omp for
      for(i = 0; i < 16; i++)
      {
         array[i] = (i%4) * tid;
      }

      //compute the sum
      sum = 0;
      for(i = 0; i < 16; i++)
         sum += array[i];
   }

   printf("The sum of all integers is : %d\n", sum);
   return 0;
}
```

## Where to go from here?

OpenMP has a lot more directives and functions needed in some situations.
To learn more, the development team for OpenMP has provided a cheat sheet available at:
[OpenMP Reference guide](https://www.openmp.org/wp-content/uploads/OpenMP-4.5-1115-CPP-web.pdf)


{% include links.md %}
