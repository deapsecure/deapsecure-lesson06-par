---
title: "PUF Modeling: OpenMP Program"
teaching: 0
exercises: 0
questions:
- "Key question (FIXME)"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "First key point. Brief Answer to questions. (FIXME)"
---

## Important Consideration for Correct Parallelization

In this episode, we will be parallelizing the PUF generation using OpenMP.
In any parallelization effort, there are several important issues
to always be aware of:

1. **Private or shared data**---is a variable supposed to be shared among threads?
   Or is a thread supposed to have its own copy of the variable?
   Although OpenMP realizes a *shared memory* programming model, not all variables
   can be shared if the program is to work correctly.
   We will have an example below.

2. **Race conditions**---where two or more threads execute multiple actions
   (near) simultaneously, resulting in an undesirable state of the program or data.

   A frequently used example is the use of a shared counter or the summation
   over many data points.
   Two or more threads may grab the value of the counter (accumulator) and
   update it at the same time, resulting in wrong computation.

   Program containing race condition can be hard to troubleshoot, as the erroneous
   result may occur all the time.
   The outcome of the computation depends on the order of commands executed by
   the independent threads.

3. **Data dependencies**---where two or more operations must be performed in
   a specific order. In this case, one operation depends on the completion
   of a previous operation.

   An example of this is a time-dependent simulation of events, such as weather
   modeling or computer network activities.
   The state of the system must be progressed from time (t--1) to (t),
   before the state of time (t+1) can be computed.
   This means that we cannot parallelize the simulation across the time domain.
   
   Another example is the iterative optimization performed on a machine learning
   model:
   while the learning process may be sped up by forming batches of data and
   using parallel computation within a batch, the entire process has an inherent
   data dependency which limits the extent of parallelization for one model.
   Ensemble models overcome this limitation by allowing each realization in the
   ensemble to be optimized independently.

In a sequential programming model, there is only one thread executing on a CPU core,
with its own data space.
The program will be executed one command after another according to the sequence
of the commands written in the program: therefore, ensuring correct computation
boils down to arranging the commands in the correct order.
This is no longer true when there are more than one thread executing the same
segment of program: each thread is free to move forward at its own pace, which
means that the same line of program will be executed by different threads at
different instances of time.
The three points above are critical for the correctness of a parallel program.
There will be additional considerations for performance of the program,
presented in the next episode.


### PUF Generation Loop: Shared or Private Variable?

Let us return to the PUF code from previous episode:

```c
   #define NUM_BITS 128
   ...
   int epoches = 200000;
   ...
   puf_generate(NUM_BITS, puf_str_a);
   puf_generate(NUM_BITS, puf_str_b);

   for(i = 0; i < epoches; i++)
   {
      init_challenge(NUM_BITS, challenge);

      for(j = 0; j < NUM_BITS; j++)
      {
         element_multiply(NUM_BITS, puf_str_a[j], challenge, tmp);
         comparison[0] = mat_sum(NUM_BITS, tmp);
         element_multiply(NUM_BITS, puf_str_b[j], challenge, tmp);
         comparison[1] = mat_sum(NUM_BITS, tmp);
         response[j] = (int)ceil((comparison[0] - comparison[1]) / 128);
      }

      memcpy(puf1_data[0] + i * NUM_BITS, challenge, NUM_BITS * sizeof(int));
      memcpy(puf1_data[1] + i * NUM_BITS, response, NUM_BITS * sizeof(int));

      if(i % 2000 == 0)
      {
         printf("Epoch %d\n", i);
      }
   }
```

On this code, it is rather obvious that the bulk of the work will be
in the loop over `i` (200000 iterations).
Therefore we need to parallelize this loop using `#pragma omp parallel for`.

If we have four threads to execute this loop, then:

* thread 0 will take on iterations `i=0` through `i=49999`

* thread 1 will take on iterations `i=50000` through `i=99999`

* ..and so on

The first question to ask is: which variables are to be shared, and which
are to be made private?

* Clearly, loop variables (`i` and `j`) must be made private.
  OpenMP is actually smart enough to recognize that `i` is private, because it will be
  assigned different values; so by default it is private.

* The variables `challenge`, `tmp`, `comparison`, and `response` are temporary
  variables, as they are defined and used only in the loop.
  Every loop will have different values for these; so they better be private
  variables as well.

* The matrices `puf_str_a` and `puf_str_b` are defined before the loop and used
  within the loop.
  All iterations *share* the same A and B matrices, therefore they are shared
  variables.

* The variables `puf1_data[0]` and `puf1_data[1]` store all the generated outputs
  (challenges and responses) from all iterations---therefore they are shared variables.

*EXERCISE*: Please convince yourself whether the choices above are correct.

*NOTE*: Local variables within subroutines are all private variables,
unless they are declared as `static` variables (not common).

Armed with these choices, we can implement the following parallelization strategy:

```c
   #pragma omp parallel for private(i, j, challenge, tmp, comparison, response)
   for(i = 0; i < epoches; i++)
   {
      ...
   }
```

This code can be compiled and tested. You can edit `puf.c`, add the directive
above, and save under a different file name, for example `puf_openmp.c`.
(Note: File `puf_omp0.c` in your hands-on directory is essentially identical to this
first modification.)

```bash
$ gcc -fopenmp -o puf_openmp puf_openmp.c -lm
```

Set the number of threads to 1 and run:
```bash
$ setenv OMP_NUM_THREADS 1
$ ./puf_openmp
```

Note the timing (CPU & wall times) and compare to the previous run.

> ## Running Multiple Threads
>
> Run the same test two more times, with 2 and 4 threads, respectively.
>
> * How did the timing change?
>
> * Can you explain the observed timing and program output?
>
> * Save the generated `puf1_result.txt` for every run. Compare the outputs
>   from different runs: are they identical.
>   (Hint: GNU diff can help with this:
>
>   ```bash
>   $ diff -qs FIRST_PUF_RESULT.txt SECOND_PUF_RESULT.txt
>   ```
>
>   Diff will print a line stating whether the two files are identical.
>
>> ## Solution
>>
>> The timing below are representative, but your actual timings may be different,
>> but should not be far off from these.
>>
>> * 1 thread:  Total time 50 s CPU, 51 s elapsed
>> * 2 threads: Total time 52 s CPU, 26 s elapsed
>> * 4 threads: Total time 53 s CPU, 13 s elapsed
>>
>> The Epoch output is also different for different number of threads:
>> For two-thread case, it can be like this:
>>
>>     Epoch 0
>>     Epoch 100000
>>     Epoch 102000
>>     Epoch 2000
>>     Epoch 104000
>>     Epoch 4000
>>     Epoch 106000
>>     Epoch 6000
>>     ...
>>
>> For four-thread case, a possible output is:
>>
>>     Epoch 0
>>     Epoch 100000
>>     Epoch 50000
>>     Epoch 150000
>>     Epoch 102000
>>     Epoch 52000
>>     Epoch 152000
>>     Epoch 2000
>>     ...
>>
>> Can you explain this?
>>
>{: .solution}
{: .challenge}

So far we have parallelized the code, and using multiple cores we were able to
obtain nearly perfect speedup!

Are we happy yet?
But we have not addressed the other two possible problems: race conditions and
data dependencies.
A quick look at the code shows that every iteration is independent, therefore
we do not have data dependency issue.
What about race condition?
This is where things get tricky.
The problem lies in the `init_challenge` function:

```c
static void init_activation(int n, int* res)
{
   int j;
   int R;
   for (j = 0; j < n; j++)
   {
      R = rand();
      if (R >= RAND_MAX / 2)
         res[j] = 1;
      else
         res[j] = 0;
   }
}
```

The (pseudo)random number generator `rand` is a function that has a global state
(in other word, it has some shared variables behind it).
As such, it is not safe for two threads to call `rand` at the same time:
it may mess up the internal generator state variables and destroy the proper
operation of the function.
(Indeed, the [rand manual page](https://linux.die.net/man/3/rand) says so.)
Random number generation is a very sequential process: random numbers come in
a definite sequence.
The initial seeding of the generator (which is done in this program) ensures
that the output of the program never changes.
Another problem is, even if the generator itself is thread-safe,
we will still be pulling different sequences of numbers for every iteration,
and there is no way to ensure reproducibility from run to run.
This is a very difficult position for scientific program to be in---because
it makes it very difficult to troubleshoot (debug) in case there is a problem.

## Taking Care of Race Conditions

To correct for the race condition, there are several strategies that can be done:

* Enclose the call to `rand` function within the `OMP CRITICAL` section.
  When multiple threads are about to process this section of the program,
  only one thread is allowed to run through this section.
  All other threads will be forced to wait until that one thread leaves
  the critical section.

* Have the master thread (that with thread ID = 0) generate a fairly large set of
  random numbers *outside* the worksharing region,
  then all threads work together using the generated random numbers.

* Use many indepedent random number generators (each with its own random number
  stream that is distinct from all the others). 
  This requires a careful appraoch to initialize each generator so
  that there is no overlap between the generated sequences.
  One of such libraries is [SPRING](http://www.sprng.org/) by Michael Mascagni
  and his colleagues.

Each strategy has its own costs:

* An `OMP CRITICAL` section is computationally expensive,
  as it requires repeated locking and unlocking of the critical section
  from other threads.
  The cost becomes prohibitively high if it is used in part of the code that is
  intensively used.
  Such is the case with our PUF generation program.

* In the second approach, other threads would be idle while the master
  thread is busy generating random numbers.
  This approach will also require an additional memory space to store the
  generated numbers.
  The impact of the program execution time (cf. Amdahl's law earlier)
  depends on how much time the master needs to work alone, relative to
  the time to do the parallelizable section of the program.
  It is easy to obtain empirical data on this: usually we can rig the program
  by commenting out the parallelizable section and measure the time.
  Comparing this time versus the time taken to run the original program will
  tell us whether this approach is beneficial.
  In the PUF program, we observed that the time required to generate all
  the random numbers is negligible compared to the PUF response calculation;
  we will choose this approach.

* The third approach has the potential to be the best performing of all
  because it gets rid of any synchronization in the computationally-intensive
  part of the code.
  However, using many independent random number generators means that the
  parallel computation results will not reproduce the sequential results.
  A computation is reproducible only when the exact same configuration of
  random number of generators is used.
  In practice, this means the exact same parallel configuration
  (the number of threads, the division of labor among threads, etc.).
  In fact, many massively parallel stochastic algorithms use this approach.
  Statistical approach can be used to verify the correctness of calculations
  that are using different parallel configurations.
  We will leave this option as an exercise for interested readers.


The approach we choose requires an additional memory space to store a the
randomly generated set of challenges.
Here is main loop (see `puf_omp3.c` for the complete program):

```c
   int epoches = 200000;
   int iblksize = 1000;

   ...

   #pragma omp parallel \
      private(i, j, i1, i0, comparison, tmp, response)
   {  // begin parallel block

      // coarse (block-wise) loop
      for(i0 = 0; i0 < epoches; i0 += iblksize)
      {
         // finer loop
         if (i0 + iblksize <= epoches)
            i1 = i0 + iblksize;
         else
            i1 = epoches;

         #pragma omp master
         for (i = i0; i < i1; i++)
         {
            init_challenge(NUM_BITS, &challenge[(i-i0)*NUM_BITS]);
         }
         #pragma omp barrier

         #pragma omp for
         for (i = i0; i < i1; ++i)
         {
            for(j = 0; j < NUM_BITS; j++)
            {
               element_multiply(NUM_BITS, puf_str_a[j], &challenge[(i-i0)*NUM_BITS], tmp);
               comparison[0] = vec_sum(NUM_BITS, tmp);
               element_multiply(NUM_BITS, puf_str_b[j], &challenge[(i-i0)*NUM_BITS], tmp);
               comparison[1] = vec_sum(NUM_BITS, tmp);
               response[j] = (int)ceil((comparison[0] - comparison[1]) / 128);
            }

            memcpy(puf1_data[0] + i * NUM_BITS, &challenge[(i-i0)*NUM_BITS], NUM_BITS * sizeof(int));
            memcpy(puf1_data[1] + i * NUM_BITS, response, NUM_BITS * sizeof(int));

            if(i % 2000 == 0)
               printf("Epoch %d\n", i);         
         } // finer "i" loop
      } // coarse "i0" loop

   } // End of OMP PARALLEL section
```

Instead of using `OMP PARALLEL FOR`, we use the plain `OMP PARALLEL` here.
The difference is that the first directive enters into the worksharing mode right away
(i.e. it expects a for loop and will divide the work among the worker threads);
the second directive enters into a parallel execution mode where all threads will
be awaken to execute the same command.
In this case, the `for i0` loop will be executed by all threads identically.
As you can see, the `OMP MASTER` encloses the loop that generates all the random numbers
(`init_challenge` function call).
There is a `OMP BARRIER` before all threads are ready to do their actual work in
the inner loop.
*It is the work in this inner loop that is partitioned among the threads.*
The original code has 1000 as the block size.
