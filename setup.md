---
title: Setup
---
<!-- 
## Pre-workshop Survey

Please fill out the pre-workshop survey first!

![Pre-workshop survey QR](fig/QR-pre-workshop.png)

### Survey link: <https://odu.co1.qualtrics.com/jfe/form/SV_aauTDnt74T3NKEl>

### Workshop URL: <https://wirawan0.gitlab.io/deapsecure-lesson06-par>
-->

## Setting Up Files

### Hands-on Files on Turing

Hands-on files are located on Turing on this subdirectory:

    /scratch-lustre/DeapSECURE/module-par/Exercises

Copy the entire directory tree to your `~/CItraining/module-par`:

    $ mkdir -p ~/CItraining/par

    $ cd ~/CItraining/par

    $ cp -pr /scratch-lustre/DeapSECURE/module-par/Exercises/. .

(Don't forget the last period!)


## Obtaining Compute Resource

In this workshop we will experiment with different configurations for process placement.
We will need to get a set of compute cores all from the same node for one configuration, and also cores from different compute nodes.

To request four cores from the same node do:

```bash
salloc -n 4 -N 1
```

To request CPU cores from different nodes do:

```bash 
salloc -n 4 --ntasks-per-node=1
```


## Setting Up Environment

Modules needed for today's experiments are:

    module use /scratch-lustre/DeapSECURE/lmod
    module load DeapSECURE
    module load icc/18 impi/18
    module load python/3.6
    module load mpi4py

We created a shell command file named `parallel-prog-env` in your hands-on
directory to ease reloading of these modules later on.
To take advantage of this, please issue this command in the same directory as before:

    $ source parallel-prog-env

Do this only once per shell session, right after you obtain a compute core.

<!--
## Post-workshop Survey

Please complete the post-workshop survey before you leave!

![Post-workshop survey QR](fig/QR-post-workshop.png)

### Survey link: <https://odu.co1.qualtrics.com/jfe/form/SV_9T996KAOnifVVtP>

### Workshop URL: <https://wirawan0.gitlab.io/deapsecure-lesson06-par>
-->

Thank you for your participation in the DeapSECURE workshops!
We hope that these workshops have enriched your knowledge on the computational
capabilities that you can use for cybersecurity work.

Please share your thoughts in the feedback:

* What would you like to see in the future version of the workshop / module?

* What materials, what topics would you like to see covered?

{% include links.md %}
